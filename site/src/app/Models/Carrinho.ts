import { Frete } from './Frete';

export class Carrinho {
    Id: number;
    Fretes: Frete[];
    PrecoTotal: number;
}
