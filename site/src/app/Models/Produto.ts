export class Produto {
    Id: number;
    Nome: string;
    Cep: string;
    Preco: number;
    Altura: number;
    Largura: number;
    Comprimento: number;
    Peso: number;
    Percentual: number;
    Vendedor: number;
    Imagem: string;
}
