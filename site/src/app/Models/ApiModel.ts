import { Produto } from './Produto';
import { Carrinho } from './Carrinho';

export class ApiModel {
    produto: Produto;
    carrinho: Carrinho;
    cep: string;
}
