import { Produto } from './Produto';

export class Frete {
    Id: number;
    Produtos: Produto[];
    ValorFrete: number;
    ValorTotalProdutos: number;
    CepOrigem: string;
    CepDestino: string;
    PesoTotal: number;
    AlturaTotal: number;
    LarguraTotal: number;
    ComprimentoTotal: number;
}
