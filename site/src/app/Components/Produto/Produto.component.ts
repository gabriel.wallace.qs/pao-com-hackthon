import { Component, OnInit } from '@angular/core';
import { ProdutoService } from 'src/app/Services/produto.service.service';
import { ApiModel } from 'src/app/Models/ApiModel';
import { Produto } from 'src/app/Models/Produto';
import { Carrinho } from 'src/app/Models/Carrinho';
import { CarrinhoService } from 'src/app/Services/carrinho.service.service';

@Component({
  selector: 'app-Produto',
  templateUrl: './Produto.component.html',
  styleUrls: ['./Produto.component.css']
})
export class ProdutoComponent implements OnInit {

  produtos: any;
  apimodel:ApiModel;

  constructor(
    private produtoService: ProdutoService,
    private carrinhoService: CarrinhoService
  ) { }

  ngOnInit() {
    this.produtoService.getProdutos()
      .subscribe(res => this.produtos = res);
  }

  adicionarAoCarrinho(produtoModel: Produto){
    this.apimodel.produto = produtoModel;
    // this.apimodel.carrinho = this.carrinhoService.getCarrinho();

    if (this.apimodel.carrinho == null){
      this.apimodel.carrinho = new Carrinho();
      this.apimodel.carrinho.Id = 1;
    }

    this.produtoService.adicionarProduto(this.apimodel);
  }

}
