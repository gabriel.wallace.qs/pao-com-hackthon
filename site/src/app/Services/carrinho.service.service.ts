import { Injectable } from '@angular/core';
import { HttpClient } from 'selenium-webdriver/http';
import { Produto } from '../Models/Produto';
import { Carrinho } from '../Models/Carrinho';

@Injectable({
  providedIn: 'root'
})
export class CarrinhoService {

  baseUrl: string = "localhost:5000/api/";

  constructor(
    private httpService: HttpClient
  ) { }

  adicionarAoCarrinho(produto: Produto, carrinho: Carrinho) {

  }

  getCarrinho() {
    const carrinho = localStorage.getItem('carrinho');

    return carrinho;
  }

}
