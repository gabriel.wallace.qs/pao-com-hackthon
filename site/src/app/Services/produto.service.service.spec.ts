/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Produto.serviceService } from './produto.service.service';

describe('Service: Produto.service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Produto.serviceService]
    });
  });

  it('should ...', inject([Produto.serviceService], (service: Produto.serviceService) => {
    expect(service).toBeTruthy();
  }));
});
