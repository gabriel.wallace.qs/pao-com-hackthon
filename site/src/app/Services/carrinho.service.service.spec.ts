/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Carrinho.serviceService } from './carrinho.service.service';

describe('Service: Carrinho.service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Carrinho.serviceService]
    });
  });

  it('should ...', inject([Carrinho.serviceService], (service: Carrinho.serviceService) => {
    expect(service).toBeTruthy();
  }));
});
