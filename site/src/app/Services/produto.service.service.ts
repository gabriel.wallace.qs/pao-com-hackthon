import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiModel } from '../Models/ApiModel';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProdutoService {

baseUrl: string = "localhost:5000/api/carrinho/";

constructor(
  private http: HttpClient
) { }

getProdutos() {
  return this.http.get('../../assets/data/Produtos.json')
}

adicionarProduto(model: ApiModel){
  this.http.post(`${this.baseUrl}adicionarProduto`,model).pipe(
    map((response: any) => {
      const carrinho = response;
      if (carrinho) {
        localStorage.setItem('carrinho', carrinho);
      }
    })
  );
}

}
