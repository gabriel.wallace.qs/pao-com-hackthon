import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './Components/app-routing.module';
import { AppComponent } from './Components/app.component';
import { CarrinhoComponent } from './Components/Carrinho/Carrinho.component';
import { ProdutoComponent } from './Components/Produto/Produto.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
   declarations: [
      AppComponent,
      CarrinhoComponent,
      ProdutoComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
