using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Text;
using System.Net;
using System.IO;
using api.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace api.Utils
{
    public class ChamadaCorreio
    {
        public Carrinho ChamarApiCorreios(Carrinho carrinho)
        {
            var _carrinho = new Carrinho();
            _carrinho.Fretes = new List<Frete>();

            foreach (var item in carrinho.Fretes)
            {
                string uri = String.Format("http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?"
                        + "nCdEmpresa={0}" 
                        + "&sDsSenha={1}"
                        + "&sCepOrigem={2}"
                        + "&sCepDestino={3}"
                        + "&nVlPeso={4}"
                        + "&nCdFormato={5}"
                        + "&nVlComprimento={6}"
                        + "&nVlAltura={7}"
                        + "&nVlLargura={8}"
                        + "&sCdMaoPropria={9}"
                        + "&nVlValorDeclarado={10}"
                        + "&sCdAvisoRecebimento={11}"
                        + "&nCdServico={12}"
                        + "&nVlDiametro={13}"
                        + "&StrRetorno=xml"
                        + "&nIndicaCalculo=3",

                "","", item.CepOrigem, 
                item.CepDestino, item.PesoTotal, "1",
                item.ComprimentoTotal, item.AlturaTotal, item.LarguraTotal,
                "N", item.ValorTotalProdutos, "n", "04014","0" );

                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                if (response.StatusCode.Equals(HttpStatusCode.OK))
                {
                    Stream stream = response.GetResponseStream();
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);

                    var xml = readStream.ReadToEnd();
                    Servicos retornoApiCorreio = ServicosXML.DeserializeXML<Servicos>(xml);
                    item.ValorFrete = double.Parse(retornoApiCorreio.cServico.Valor);
                    item.PrazoPrevisto = int.Parse(retornoApiCorreio.cServico.PrazoEntrega.ToString());

                    _carrinho.Fretes.Add(item);
                }
                else
                {
                    Console.WriteLine("Erro ao adicionar valores da API dos correios");
                }

            }

            return _carrinho;
        }      

        public Carrinho AtualizarVendedores(Carrinho carrinho)
        {
            if(carrinho.Vendedores == null)
            {
                carrinho.Vendedores = new List<Vendedor>();
            }
            foreach (var frete in carrinho.Fretes)
            {
                foreach (var produto in frete.Produtos)
                {
                    int i;
                    for (i = 0; i < carrinho.Vendedores.Count(); i++)
                    {
                        if(carrinho.Vendedores[i].Nome.Equals(produto.Vendedor))
                        {
                            carrinho.Vendedores[i].Total += produto.Preco * produto.Percentual;
                            break;
                        }
                    }
                    if(i == carrinho.Vendedores.Count())
                    {
                        Vendedor tmp = new Vendedor();
                        tmp.Nome = produto.Vendedor;
                        tmp.Total = produto.Preco * produto.Percentual;
                        carrinho.Vendedores.Add(tmp);
                    }
                }  
            }

            Vendedor dono = new Vendedor();
            dono.Nome = "Site";
            dono.Total = carrinho.Fretes.Sum(preco => preco.ValorTotalProdutos) - carrinho.Vendedores.Sum(desc =>desc.Total);
            carrinho.Vendedores.Add(dono);

            return carrinho;
        }
        
    }
    
}