﻿using System;

namespace api.Dtos
{
    public class Produto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cep { get; set; }
        public string Regiao { get; set; }
        public double Preco { get; set; }
        public double Altura { get; set; }
        public double Largura { get; set; }
        public double Comprimento { get; set; }
        public double Peso { get; set; }
        public double Percentual { get; set; }
        public string Vendedor { get; set; }
        public string Imagem { get; set; }
    }
}
