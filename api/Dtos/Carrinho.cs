using System;
using System.Collections.Generic;

namespace api.Dtos
{
    public class Carrinho
    {
        public int Id { get; set; }
        public List<Frete> Fretes { get; set; }     
        public double PrecoTotal { get; set; }
        public List<Vendedor> Vendedores { get; set; }
    }
}