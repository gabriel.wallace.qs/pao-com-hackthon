namespace api.Dtos
{
    public class Vendedor
    {
        public string Nome { get; set; }
        public double Total { get; set; }
    }
}