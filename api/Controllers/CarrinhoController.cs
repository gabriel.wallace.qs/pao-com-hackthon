using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Dtos;
using api.Model;
using api.Utils;
using api.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers {
    [ApiController]
    [Route ("api/[controller]")]
    public class CarrinhoController : ControllerBase {

        [HttpGet ("GetCarrinho")]
        public async Task<IActionResult> GetCarrinho () {
            return Ok (
                new CarrinhoModel {
                    Produto = new Produto {
                            Id = 0,
                                Nome = "",
                                Cep = "",
                                Regiao = "",
                                Preco = 0,
                                Altura = 0,
                                Largura = 0,
                                Comprimento = 0,
                                Peso = 0,
                                Percentual = 0,
                                Vendedor = "",
                                Imagem = ""
                        },
                        Carrinho = new Carrinho {
                            Id = 0,
                                PrecoTotal = 0,
                                Fretes = new List<Frete> {
                                    new Frete {
                                        Id = 0,
                                            AlturaTotal = 0,
                                            CepDestino = "",
                                            CepOrigem = "",
                                            ComprimentoTotal = 0,
                                            LarguraTotal = 0,
                                            PesoTotal = 0,
                                            ValorFrete = 0,
                                            ValorTotalProdutos = 0,
                                            PrazoPrevisto = 0,
                                            Produtos = new List<Produto> {
                                                new Produto {
                                                    Id = 0,
                                                        Nome = "",
                                                        Cep = "",
                                                        Regiao = "",
                                                        Preco = 0,
                                                        Altura = 0,
                                                        Largura = 0,
                                                        Comprimento = 0,
                                                        Peso = 0,
                                                        Percentual = 0,
                                                        Vendedor = "",
                                                        Imagem = ""
                                                }
                                            }
                                    }
                                }
                        },
                        Cep = ""
                }
            );
        }

        [HttpPost ("adicionarProduto")]
        public async Task<ActionResult> AddCarrinho (CarrinhoModel model) {

            try {
                var freteService = new FreteService (model.Carrinho);

                model.Carrinho = await freteService.AdicionaProdutoAsync (model.Produto);

                return Ok (model.Carrinho);
            } catch (Exception ex) {
                return this.StatusCode (StatusCodes.Status500InternalServerError,
                    "Banco de Dados falhou!\n" +
                    $"Detalhes do erro: {ex.Message}");
            }

        }

        [HttpGet("calcular")]
        public Carrinho CalcularFrete (CarrinhoModel model) {
            
            var correio = new ChamadaCorreio();
            FreteService fs = new FreteService(model.Carrinho);
            Carrinho carrinho;

            carrinho = fs.otimizarTamanho(model.Carrinho);

            carrinho = correio.ChamarApiCorreios(carrinho);

            carrinho = correio.AtualizarVendedores(carrinho);
            
            return carrinho;
        }
    }
}