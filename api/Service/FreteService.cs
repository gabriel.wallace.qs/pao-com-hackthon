using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Dtos;

namespace api.Service
{
    public class FreteService
    {
        private readonly Carrinho _carrinho;

        public FreteService(Carrinho carrinho)
        {
            _carrinho = carrinho;
        }

        public Carrinho CalcularFrete(List<Produto> produtos, string cep)
        { //VINICIUS

            //separaListaPorCep(produtos, cep);

            //Utils.chamaApiCorreios();

            return _carrinho;
        }

        public async Task<Carrinho> AdicionaProdutoAsync(Produto produto)
        { //VINICIUS

            var fretes = _carrinho.Fretes;
            var frete = new Frete();

            foreach (var item in fretes)
            {
                if (item.CepOrigem == produto.Cep)
                {
                    frete = item;
                }
            }
            
            if( frete.Produtos == null){
                frete.Produtos = new List<Produto>();
            }

            frete.Produtos.Add(produto);
            
            if (fretes.Exists (x => x == frete)) {
                fretes.Remove (frete);
                fretes.Add (frete);
            } else {
                frete.Id = int.Parse(produto.Cep);
                frete.CepOrigem = produto.Cep;
                fretes.Add (frete);

            }

            return _carrinho;
        }

        public Carrinho otimizarTamanho(Carrinho carrinho)
        { //VINICIUS
            //Fazer otimização do tamanho do frete para unico CEP
            Carrinho ret = carrinho;
            double LimT = 200.0, LimA = 105.0, LimL = 105, LimC = 105, LimP = 30;
            foreach (var frete in carrinho.Fretes)
            {
                frete.Produtos.Sort((x, y) => x.Largura.CompareTo(y.Largura));
                double LarguradoPacote = frete.Produtos.First().Largura;
                frete.Produtos.Sort((x, y) => x.Comprimento.CompareTo(y.Comprimento));
                double ComprimentodoPacote = frete.Produtos.First().Comprimento;
                double AlturadoPacote = frete.Produtos.Select(i => i.Altura).Sum();
                double PesoPacote = frete.Produtos.Select(i => i.Peso).Sum();
                while (AlturadoPacote > LimA || ComprimentodoPacote + LarguradoPacote + AlturadoPacote > LimT ||PesoPacote > LimP)
                {
                    Frete tmp = new Frete();
                    tmp = frete;
                    tmp.Id += 1;
                    for (int i = 0; i < frete.Produtos.Count(); i++)
                    {
                        if (i % 2 == 1)
                            frete.Produtos.Remove(frete.Produtos.ElementAt(i));
                        else
                            tmp.Produtos.Remove(frete.Produtos.ElementAt(i));
                    }
                    carrinho.Fretes.Add(tmp);
                    frete.Produtos.Sort((x, y) => x.Largura.CompareTo(y.Largura));
                    LarguradoPacote = frete.Produtos.First().Largura;
                    frete.Produtos.Sort((x, y) => x.Comprimento.CompareTo(y.Comprimento));
                    ComprimentodoPacote = frete.Produtos.First().Comprimento;
                    AlturadoPacote = frete.Produtos.Select(i => i.Altura).Sum();
                    PesoPacote = frete.Produtos.Select(i => i.Peso).Sum();
                }
                frete.LarguraTotal = LarguradoPacote;
                frete.ComprimentoTotal = ComprimentodoPacote;
                frete.AlturaTotal = AlturadoPacote;
                frete.PesoTotal = PesoPacote;
                frete.ValorTotalProdutos = frete.Produtos.Select(i => i.Preco).Sum();
            }

            if(carrinho.Fretes.Last().ValorTotalProdutos == 0)
                ret = otimizarTamanho(carrinho);

            return ret;
        }
    }
}