using api.Dtos;

namespace api.Model
{
    public class CarrinhoModel
    {
        public Produto Produto { get; set; }
        public Carrinho Carrinho { get; set; }
        public string Cep { get; set; }
    }
}